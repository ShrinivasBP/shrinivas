// parameter matchObj : matchData

function winsPerTeam(matchObj){

      let winnersOfYear= Object.entries(matchObj).reduce(callback,{})  

      return winnersOfYear
  }

// index =1 : object has been converted to array so keys are available at index 1
function callback(listOfWinners, winningTeam) {
    let index=1

      if (winningTeam[index]['season'] in listOfWinners){

          if (winningTeam[index]['winner'] in listOfWinners[winningTeam[index]['season']]) {
              listOfWinners[winningTeam[index]['season']][winningTeam[index]['winner']]+=1;
          }
          else{
          listOfWinners[winningTeam[index]['season']][winningTeam[index]['winner']] = 1;
          }

      } 

      else{
          listOfWinners[winningTeam[index]['season']] = {};
          listOfWinners[winningTeam[index]['season']][winningTeam[1]['winner']] = 1;
      }

  return listOfWinners;
  } 

module.exports={winsPerTeam :winsPerTeam}

