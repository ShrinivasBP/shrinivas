const path= require("path")
const csv=require('csvtojson')
const matchCsvFilePath=(path.join(__dirname,"../data/matches.csv"))
const deliveriesCsvFilePath=(path.join(__dirname,'../data/deliveries.csv'))
const fs= require("fs")


csv()
.fromFile(matchCsvFilePath)
.then((matchData)=>{
    fs.writeFile(path.join(__dirname,"../../matches.json"), JSON.stringify(matchData),"utf-8",(err)=>{
        if(err){
            console.error("Error Occurred while writing into the file")
        }
        else{
            console.log("converting match.csv to Json is a success")            
        }
    })
})
.catch(()=>{
    console.error("Error while changing the formate")
})

csv()
.fromFile(deliveriesCsvFilePath)
.then((deliveriesData)=>{
    fs.writeFile(path.join(__dirname,"../../deliveries.json"), JSON.stringify(deliveriesData),"utf-8",(err)=>{
        if(err){
            console.error("Error Occurred while writing into the file")
        }
        else{
            console.log("converting deliveries.csv to Json is a success")
        }
    })
})
.catch(()=>{
    console.error("Error while changing the formate")
})

