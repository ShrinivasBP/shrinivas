
const http= require("http")
const fs= require("fs")
const path= require("path")



function readData(filePath){    
    return new Promise((resolve,reject)=>{

        fs.readFile(filePath,"utf-8",(error,fileData)=>{

            if(error){       
                console.error(error)         
                reject("Error occurred while reading the file")
            }
            else{                
                resolve((fileData))               
            }

        })
    })
}

let server= http.createServer((request,response)=>{
    
    switch(request.url){

        case "/":{
            readData(path.join(__dirname,"../public/index.html"))

            .then((html)=>{
                response.writeHead(200,{"Content-Type": "text/html"})
                response.write(html)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })  

        break;
        }

        case "/noOfMatches":{
            readData(path.join(__dirname,"../public/HTML/noOfMatches.html"))

            .then((html)=>{
                response.writeHead(200,{"Content-Type": "text/html"})
                response.write(html)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })     

        break;
        }
        case "/noOfMatches.css":{
            readData(path.join(__dirname,"../public/CSS/noOfMatches.css"))

            .then((css)=>{
                response.writeHead(200,{"Content-Type": "text/css"})
                response.write(css)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })     

        break;
        }

        case "/noOfMatches.js":{
            readData(path.join(__dirname,"../public/JavaScript/noOfMatches.js"))

            .then((js)=>{
                response.writeHead(200,{"Content-Type": "text/js"})
                response.write(js)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })    

        break;
        }

        case "/noOfMatches.json":{
            readData(path.join(__dirname,"../public/output/matchesPerYear.json"))

            .then((json)=>{
                response.writeHead(200,{"Content-Type": "application/json"})
                response.write(json)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })

            break;        
        }

        case "/extrasInYear":{
            readData(path.join(__dirname,"../public/HTML/extrasInYear.html"))

            .then((html)=>{
                response.writeHead(200,{"Content-Type": "text/html"})                
                response.write(html)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })

            break;
        }

        case "/extrasInYear.css":{
            readData(path.join(__dirname,"../public/CSS/extrasInYear.css"))

            .then((css)=>{
                response.writeHead(200,{"Content-Type": "text/css"})
                response.write(css)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })     

        break;
        }

        case "/extrasInYear.js":{
            readData(path.join(__dirname,"../public/JavaScript/extrasInYear.js"))

            .then((js)=>{
                response.writeHead(200,{"Content-Type": "text/js"})            
                response.write(js)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end
            })

            break;
        }

        case "/extrasInYear.json":{
            readData(path.join(__dirname,"../public/output/extrasInYear.json"))

            .then((json)=>{
                response.writeHead(200,{"Content-Type": "application/json"})                
                response.write(json)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })

            break;
        }

        case "/topEcoBowlers":{
            readData(path.join(__dirname,"../public/HTML/topEcoBowlers.html"))

            .then((html)=>{
                response.writeHead(200,{"Content-Type": "text/html"})                
                response.write(html)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end
            })

            break;
        }

        case "/topEcoBowlers.css":{
            readData(path.join(__dirname,"../public/CSS/topEcoBowlers.css"))

            .then((css)=>{
                response.writeHead(200,{"Content-Type": "text/css"})
                response.write(css)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })     

        break;
        }

        case "/topEcoBowlers.js":{
            readData(path.join(__dirname,"../public/JavaScript/topEcoBowlers.js"))
            
            .then((js)=>{
                response.writeHead(200,{"Content-Type": "text/js"})                
                response.write(js)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })            

            break;
        }
        
        case "/topEcoBowlers.json":{
            readData(path.join(__dirname,"../public/output/topTenEconomicalBowlers.json"))

            .then((json)=>{
                response.writeHead(200,{"Content-Type": "application/json"})               
                response.write(json)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })            

            break;
        }

        case "/winsPerTeam":{
            readData(path.join(__dirname,"../public/HTML/winsPerTeam.html"))
            
            .then((html)=>{
                response.writeHead(200,{"Content-Type": "text/html"})                
                response.write(html)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })            

            break;
        }

        case "/winsPerTeam.css":{
            readData(path.join(__dirname,"../public/CSS/winsPerTeam.css"))

            .then((css)=>{
                response.writeHead(200,{"Content-Type": "text/css"})
                response.write(css)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })     

        break;
        }

        case "/winsPerTeam.js":{
            readData(path.join(__dirname,"../public/JavaScript/winsPerTeam.js"))
            
            .then((js)=>{
                response.writeHead(200,{"Content-Type": "text/js"})                
                response.write(js)
                response.end()
            })
            
            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()
            })  

            break;

        }

        case "/winsPerTeam.json":{
            readData(path.join(__dirname,"../public/output/winsPerYear.json"))

            .then((json)=>{
                response.writeHead(200,{"Content-Type": "application/json"})               
                response.write(json)
                response.end()
            })

            .catch(()=>{
                response.writeHead(404)
                response.write("Error : file not found")
                response.end()                
            })            

            break;

        }

        default : {

            response.writeHeader(404) , {'content-type': 'text/html'};
            response.write("Page not found");
            response.end();

        }
    }  
    
})
server.listen(8000,(error)=>{

    if(error){
        console.log(error)
    }
    else{
        console.log("server is listening at port 8000")
    }

})

