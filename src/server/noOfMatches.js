
//matchArr : matchData
function matches(matchArr){
    let matches=(matchArr).reduce(((totalMatches,match)=>{
            
        if(totalMatches[match['season']]== undefined){
            totalMatches[match['season']]= 1
        }
        else{
            totalMatches[match['season']]+=1
        }
        return totalMatches
    }),{})

    return matches
}


module.exports={matches : matches}