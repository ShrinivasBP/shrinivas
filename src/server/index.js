const path = require("path")
const MATCHES_FILE_PATH = (path.join(__dirname,"../../matches.json"))
const DELIVERIES_FILE_PATH = (path.join(__dirname,"../../deliveries.json"))
const iplMatch = require(path.join(__dirname,"noOfMatches.js"))
const iplWins = require(path.join(__dirname,"winsInYear.js"))
const iplExtras = require(path.join(__dirname,"extrasInYear.js"))
const iplEcoBowlers = require(path.join(__dirname,"topEcoBowlers.js"))
const fs = require("fs")

// paths of output files
const NUM_OF_MATCHES_FILE_PATH = path.join(__dirname,"../public/output/matchesPerYear.json")
const WINS_OF_TEAM_FILE_PATH = path.join(__dirname,"../public/output/winsPerYear.json")
const EXTRAS_CONCEDED_FILE_PATH = path.join(__dirname,"../public/output/extrasInYear.json")
const TOP_TEN_BOWLERS_FILE_PATH = path.join(__dirname,"../public/output/topTenEconomicalBowlers.json")


function readData(filePath){    
    return new Promise((resolve,reject)=>{

        fs.readFile(filePath,"utf-8",(error,matchData)=>{

            if(error){       
                console.error(error)         
                reject("Error occurred while reading the file")
            }
            else{                
                resolve(JSON.parse(matchData))               
            }

        })
    })
}


function writeData(fileData, resultFilePath){
    return new Promise((resolve,reject)=>{

        fs.writeFile(resultFilePath,JSON.stringify(fileData),"utf-8",(error)=>{

            if(error){
                reject((error))
            }
            else{
                resolve(("success writing file to destination @ "+ resultFilePath ))
            }
            
        })
    })
}



// Number of matches 

readData(MATCHES_FILE_PATH)
.then((matchesArr)=>{
    return matchResult= iplMatch.matches(matchesArr)
})

.then((noOfWins)=>{
   writeData(noOfWins, NUM_OF_MATCHES_FILE_PATH)

   .then((message)=>{
       console.log(message)
   })
   .catch((error)=>{
       console.error(error)
   })

})
.catch((error)=>{
    console.error(error)
})



// Wins of Teams every year

readData(MATCHES_FILE_PATH)
.then((matchesArr)=>{
    return winsResult= iplWins.winsPerTeam(matchesArr)
})

.then((winsOfTeam)=>{    
   writeData(winsOfTeam, WINS_OF_TEAM_FILE_PATH)

   .then((message)=>{
       console.log(message)
   })
   .catch((error)=>{
       console.error(error)
   })

})
.catch((error)=>{
    console.error(error)
})



//Extras in the year 2016

const YEAR_FOR_EXTRAS="2016"
readData(MATCHES_FILE_PATH)
.then((matchesArr)=>{   
    return limitResult= Promise.all([readData(DELIVERIES_FILE_PATH),iplExtras.returnId(matchesArr, YEAR_FOR_EXTRAS)])   
})

.then((arrOfLimitAndDeliveries)=>{
    const DELIVERIES_DATA= arrOfLimitAndDeliveries[0]
    const MATCH_ID = arrOfLimitAndDeliveries[1]
    return extraResults = iplExtras.extraRuns(DELIVERIES_DATA, MATCH_ID)
})

.then((extraRuns)=>{    
   writeData(extraRuns, EXTRAS_CONCEDED_FILE_PATH)

   .then((message)=>{
       console.log(message)
   })
   .catch((error)=>{
       console.error(error)
   })

})
.catch((error)=>{
    console.error(error)
})



// Top 10 economical bowlers in 2015

const YEAR_FOR_ECONOMICAL_BOWLERS="2015"
readData(MATCHES_FILE_PATH)
.then((matchesArr)=>{
    return Promise.all([readData(DELIVERIES_FILE_PATH),iplExtras.returnId(matchesArr, YEAR_FOR_ECONOMICAL_BOWLERS)]) 
})

.then((arrOfDeliveriesAndLimit)=>{
    const DELIVERIES_DATA= arrOfDeliveriesAndLimit[0]
    const MATCH_ID = arrOfDeliveriesAndLimit[1]
    return iplEcoBowlers.economicalBowlers(DELIVERIES_DATA, MATCH_ID)
})

.then((economicalBowlers)=>{    
   writeData(economicalBowlers, TOP_TEN_BOWLERS_FILE_PATH)

   .then((message)=>{
       console.log(message)
   })
   .catch((error)=>{
       console.error(error)
   })
   
})
.catch((error)=>{
    console.error(error)
})