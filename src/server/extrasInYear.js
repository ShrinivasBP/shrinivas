//matchObj : match Data
function returnId(matchObj,year){    

    let id=matchObj.filter(idOfYear =>idOfYear["season"]== year)
    let arr=id.map(match => match["id"])
    let limits=[parseInt(arr[0]),parseInt(arr[arr.length-1])]
    return limits

}


//deliveriesObj : deliveries data, 
// id : the limits returned by returnId() 
function extraRuns(deliveriesObj,id){

    startId=id[0];
    endId=id[1];
    let extraRuns=Object.entries(deliveriesObj).reduce(callback,{})    
    return extraRuns

}


function callback(table, over){
    let index=1;

    if(over[index]["match_id"]>=startId && over[index]["match_id"]<=endId){

        if(table[over[index]["bowling_team"]]){
            table[over[index]["bowling_team"]]+=parseInt(over[index]["extra_runs"])
        }
        else{
            table[over[index]["bowling_team"]]= parseInt(over[index]["extra_runs"])
        }
        
    }    

    return table
}

module.exports={
    returnId : returnId,
    extraRuns : extraRuns
}