/* deliveries : deliveries Data, 
id : the function has been written in extrasInYear.js to which returns id of start and end match_Id of the year as array
index=1 object is converted to array
*/
function economicalBowlers(deliveries, id){    
    let index=1, runs=0 ,balls=1
    let startId=id[0]    
    let endId=id[id.length-1]
    let economicalBowlers=Object.entries(deliveries).reduce( 
      ((bowlerList,bowler)=>{

        if(bowler[index]['match_id']>=startId && bowler[index]['match_id']<=endId){

            if(bowler[index]['bowler'] in bowlerList){                
                bowlerList[bowler[index]['bowler']][balls]+=1
                bowlerList[bowler[index]['bowler']][runs]=bowlerList[bowler[index]['bowler']][runs]+parseInt((bowler[index]['total_runs']))    
            }    
            else{
            bowlerList[bowler[index]['bowler']]=[]
            bowlerList[bowler[index]['bowler']][balls]=1;
            bowlerList[bowler[index]['bowler']][runs]=parseInt(bowler[index]['total_runs']);    
          }

        }   

        return bowlerList    
      }),{})
    
      Object.keys(economicalBowlers).map(details=>economicalBowlers[details]=
          (economicalBowlers[details][runs] / (economicalBowlers[details][balls]/6)).toFixed(2))    
    
    let topTenBowlers=Object.fromEntries(Object.entries(economicalBowlers).sort((a,b)=>a[1]-b[1]).splice(0,10))    
   
    return topTenBowlers  
}

module.exports= {economicalBowlers :economicalBowlers}