function data(){
    fetch("noOfMatches.json")
    .then((file)=>{
        return file=file.json()
        
    }).then((data)=>{
        let res=[]
        for(let ele in data){
            res.push([ele,data[ele]])
            //console.log(data[ele])
            //console.log(ele)
        }
        return (res)
    }).then((result)=>{
        chart(result)
        //console.log(result)
    })
}

data()

function chart(mappingdata){
  Highcharts.chart('container', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: 0,
      plotShadow: false
    },
    title: {
      text: 'Matches<br>shares<br>of IPL from 2008- 2017',
      align: 'center',
      verticalAlign: 'middle',
      y: 60
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.y:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: ''
      }
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: true,
          distance: -50,
          style: {
            fontWeight: 'bold',
            color: 'white'
          }
        },
        startAngle: -90,
        endAngle: 90,
        center: ['50%', '75%'],
        size: '110%'
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      innerSize: '50%',
      data: mappingdata
    }]
  });
  
 }
 