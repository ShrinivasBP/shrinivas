function data(){
    fetch("extrasInYear.json")
    .then((file)=>{
        return file.json()
    }).then((data)=>{
        let index=[]
        let result=[]
        for(let ele in data){
            index.push(ele)
            result.push(data[ele])
        }        
        graph(index,result)
    })

}

data()
function graph(index,result){
    var chart = Highcharts.chart('container', {

        title: {
            text: 'Extra runs conceded by bowler '
        },
  
        subtitle: {
            text: 'Year : 2016'
        },
  
        xAxis: {
           categories: index
        },
  
        series: [{
            type: 'column',
            colorByPoint: true,
            data: result,
            showInLegend: false
        }]  
    });  
}
  
  