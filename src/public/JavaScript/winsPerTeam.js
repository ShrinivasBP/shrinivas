function visulaize(){
    fetch("winsPerTeam.json")
    .then((winsData)=>{
        return winsData.json()
    })
    .then((wins)=>{
        let years=[]
        let teams=[]
        let winResult=[]
        let series=[]
        for(let ele in wins){
            years.push(ele)            
            for(let team in wins[ele]){
                if(!(teams.includes(team))){
                    teams.push(team)
                }
        }        
    }
    for(let team of teams){
        let currArr=[]
        for(let year of years){
            if(wins[year][team]== undefined){
                currArr.push(0)
            }
            else{
                currArr.push(wins[year][team])
            }
        }
        winResult.push(currArr)
    }
    for(let team in teams){
        let currObj={}
        currObj["name"]=teams[team]
        currObj["data"]=winResult[team]
        series.push(currObj)
    }
    console.log(years)
    console.log(teams)
    console.log(winResult)
    chart(years,series)
    })
}

visulaize()



function chart(years,series){

Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Number of matches won per team from 2008-2017'
    },
    subtitle: {
      text: 'source: kaggle.com'
    },
    xAxis: {
      categories: years,
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Number of wins'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: series
  });
}