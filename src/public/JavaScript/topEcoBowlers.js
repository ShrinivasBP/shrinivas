function data(){
    fetch("topEcoBowlers.json")
    .then((file)=>{
        return file.json()
    })
    .then((data)=>{
        let result=[]
        for(let ele in data){
            result.push([ele, Number(data[ele])])
        }
        console.log(result)
        chart(result)
    })
}

data()

function chart(mappingData){
    Highcharts.chart('container', {
chart: {
type: 'column'
},
title: {
text: "Top Economical bowlers in 2015"
},
subtitle: {
text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle.com</a>'
},
xAxis: {
type: 'category',
labels: {
rotation: -45,
style: {
  fontSize: '13px',
  fontFamily: 'Verdana, sans-serif'
}
}
},
yAxis: {
min: 0,
title: {
text: 'Economy'
}
},
legend: {
enabled: true
},
tooltip: {
pointFormat: 'matches : <b>{point.y:.1f} </b>'
},
series: [{
name: 'Bowlers',
data: mappingData,
dataLabels: {
enabled: true,
rotation: -90,
color: '#FFFFFF',
align: 'right',
format: '{point.y:.1f}', // one decimal
y: 10, // 10 pixels down from the top
style: {
  fontSize: '13px',
  fontFamily: 'Verdana, sans-serif'
}
}
}]
});
}